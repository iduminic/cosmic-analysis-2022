#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

#for cosmics data
#testFile = "/data/iduminic/forSocratis/data16_13TeV.311365.physics_CosmicCalo.AOD.r11077_p3697/AOD.17129106._000003.pool.root.1"
testFile = "/eos/user/f/flahbabi/forSocratis/data16_13TeV.311365.physics_CosmicCalo.AOD.r11077_p3697/AOD.17129106._000003.pool.root.1"
#for all data
#testFile = "/eos/user/c/calpigia/forSocratis/data16_13TeV.00311365.physics_Main.merge.AOD.r9264_p3083/AOD.11038617._000001.pool.root.1"
#Monte Carlo
#testFile = "/eos/user/v/verducci/Tile/AOD_Cosmics/mc16_13TeV.310772.CosmicRays_CollisionSetup.merge.AOD.s3367_r10724_r10726/AOD.19461622._000001.pool.root.1"
#override next line on command line with: --filesInput=XXX
jps.AthenaCommonFlags.FilesInput = [testFile] 

#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 


# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
theApp.EvtMax = -1

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")
