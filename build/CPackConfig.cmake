# This file will be configured to contain variables for CPack. These variables
# should be set in the CMake list file of the project before CPack module is
# included. The list of available CPACK_xxx variables and their associated
# documentation may be obtained using
#  cpack --help-variable-list
#
# Some variables are common to all generators (e.g. CPACK_PACKAGE_NAME)
# and some are specific to a generator
# (e.g. CPACK_NSIS_EXTRA_INSTALL_COMMANDS). The generator specific variables
# usually begin with CPACK_<GENNAME>_xxxx.


set(CPACK_BUILD_SOURCE_DIRS "/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/source;/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/build")
set(CPACK_CMAKE_GENERATOR "Unix Makefiles")
set(CPACK_COMPONENTS_ALL "Debug;Main;Unspecified")
set(CPACK_COMPONENT_UNSPECIFIED_HIDDEN "TRUE")
set(CPACK_COMPONENT_UNSPECIFIED_REQUIRED "TRUE")
set(CPACK_DEFAULT_PACKAGE_DESCRIPTION_FILE "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.21.3/Linux-x86_64/share/cmake-3.21/Templates/CPack.GenericDescription.txt")
set(CPACK_DEFAULT_PACKAGE_DESCRIPTION_SUMMARY "UserAnalysis built using CMake")
set(CPACK_GENERATOR "RPM")
set(CPACK_INSTALL_CMAKE_PROJECTS "")
set(CPACK_INSTALL_COMMANDS "/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/build/CMakeFiles/cpack_install.sh")
set(CPACK_INSTALL_PREFIX "usr/UserAnalysis/1.0.0/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_INSTALL_SCRIPT "/cvmfs/atlas.cern.ch/repo/sw/software/22.2/AthAnalysis/22.2.76/InstallArea/x86_64-centos7-gcc11-opt/cmake/modules/scripts/cpack_install.cmake")
set(CPACK_MODULE_PATH "/cvmfs/atlas.cern.ch/repo/sw/software/22.2/AthAnalysis/22.2.76/InstallArea/x86_64-centos7-gcc11-opt/cmake/modules;/cvmfs/atlas.cern.ch/repo/sw/software/22.2/AthAnalysisExternals/22.2.76/InstallArea/x86_64-centos7-gcc11-opt/cmake/modules")
set(CPACK_NSIS_DISPLAY_NAME "UserAnalysis/1.0.0/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_NSIS_INSTALLER_ICON_CODE "")
set(CPACK_NSIS_INSTALLER_MUI_ICON_CODE "")
set(CPACK_NSIS_INSTALL_ROOT "$PROGRAMFILES")
set(CPACK_NSIS_PACKAGE_NAME "UserAnalysis/1.0.0/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_NSIS_UNINSTALL_NAME "Uninstall")
set(CPACK_OUTPUT_CONFIG_FILE "/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/build/CPackConfig.cmake")
set(CPACK_PACKAGE_CONTACT "atlas-sw-core@cern.ch")
set(CPACK_PACKAGE_DEFAULT_LOCATION "/usr")
set(CPACK_PACKAGE_DESCRIPTION "UserAnalysis - 1.0.0")
set(CPACK_PACKAGE_DESCRIPTION_FILE "/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/build/CMakeFiles/README.txt")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "UserAnalysis - 1.0.0")
set(CPACK_PACKAGE_FILE_NAME "UserAnalysis_1.0.0_x86_64-centos7-gcc11-opt")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "UserAnalysis/1.0.0/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "UserAnalysis/1.0.0/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_PACKAGE_NAME "UserAnalysis")
set(CPACK_PACKAGE_RELOCATABLE "true")
set(CPACK_PACKAGE_VENDOR "ATLAS Collaboration")
set(CPACK_PACKAGE_VERSION "1.0.0")
set(CPACK_PACKAGE_VERSION_MAJOR "1")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PACKAGE_VERSION_PATCH "0")
set(CPACK_PROJECT_CONFIG_FILE "/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/build/CMakeFiles/CPackOptions.cmake")
set(CPACK_RESOURCE_FILE_LICENSE "/cvmfs/atlas.cern.ch/repo/sw/software/22.2/AthAnalysis/22.2.76/InstallArea/x86_64-centos7-gcc11-opt/LICENSE.txt")
set(CPACK_RESOURCE_FILE_README "/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/build/CMakeFiles/README.txt")
set(CPACK_RESOURCE_FILE_WELCOME "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.21.3/Linux-x86_64/share/cmake-3.21/Templates/CPack.GenericWelcome.txt")
set(CPACK_RPM_PACKAGE_ARCHITECTURE "noarch")
set(CPACK_RPM_PACKAGE_AUTOREQ " no")
set(CPACK_RPM_PACKAGE_AUTOREQPROV " no")
set(CPACK_RPM_PACKAGE_GROUP "ATLAS Software")
set(CPACK_RPM_PACKAGE_LICENSE "Apache License Version 2.0")
set(CPACK_RPM_PACKAGE_NAME "UserAnalysis_1.0.0_x86_64-centos7-gcc11-opt")
set(CPACK_RPM_PACKAGE_PROVIDES "/bin/sh")
set(CPACK_RPM_PACKAGE_REQUIRES "AthAnalysis_22.2.76_x86_64-centos7-gcc11-opt, LCG_101_ATLAS_20_tbb_2020_U2_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_cppgsl_3.1.0_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_AIDA_3.2.1_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_HepPDT_2.06.01_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_CppUnit_1.14.0_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_libunwind_1.3.1_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_Python_3.9.6_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_jsonmcpp_3.9.1_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_zlib_1.2.11_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_fmt_7.1.3_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_XercesC_3.2.3_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_gtest_1.10.0_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_Boost_1.77.0_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_ROOT_6.24.06a_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_rangev3_0.11.0_x86_64_centos7_gcc11_opt, LCG_101_ATLAS_20_clhep_2.4.4.0_x86_64_centos7_gcc11_opt")
set(CPACK_RPM_PACKAGE_VERSION "1.0.0")
set(CPACK_RPM_SPEC_MORE_DEFINE "%global __os_install_post %{nil}")
set(CPACK_SET_DESTDIR "OFF")
set(CPACK_SOURCE_GENERATOR "RPM")
set(CPACK_SOURCE_OUTPUT_CONFIG_FILE "/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/build/CPackSourceConfig.cmake")
set(CPACK_SYSTEM_NAME "Linux")
set(CPACK_THREADS "1")
set(CPACK_TOPLEVEL_TAG "Linux")
set(CPACK_WIX_SIZEOF_VOID_P "8")

if(NOT CPACK_PROPERTIES_FILE)
  set(CPACK_PROPERTIES_FILE "/home/iduminic/data/public/Cosmics_Code/ROOTAnalysisTutorial/build/CPackProperties.cmake")
endif()

if(EXISTS ${CPACK_PROPERTIES_FILE})
  include(${CPACK_PROPERTIES_FILE})
endif()
