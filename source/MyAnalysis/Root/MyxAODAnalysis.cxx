#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"
//#include "xAODTruth/TruthVertexContainer.h"


//#include <AthenaBaseComps/AthAlgorithm.h>

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  m_jetCollection = "AntiKt4EMTopoJets";
  declareProperty("TrigDecisionTool",m_trigTool,"My trigger decision");

}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO("in initinalize");

  m_file=TFile::Open("CosmicsData.root","recreate");

  m_tree = new TTree("CollectionTree", "");
  m_treeFolder = gDirectory->GetPath(); 
  m_tree->Branch("runNumber",&m_runNumber,"runNumber/i");
  m_tree->Branch("eventNumber",&m_eventNumber,"eventNumber/i");

  m_tree->Branch("NPV",&m_NPV,"NPV/i");
  m_tree->Branch("m_L1_J12",&m_L1_J12,"L1_J12/I"); 

  m_tree->Branch("MuonSeg_x","vector<float>",&m_MuonSeg_x);
  m_tree->Branch("MuonSeg_y","vector<float>",&m_MuonSeg_y);
  m_tree->Branch("MuonSeg_z","vector<float>",&m_MuonSeg_z);
  m_tree->Branch("MuonSeg_px","vector<float>",&m_MuonSeg_px);
  m_tree->Branch("MuonSeg_py","vector<float>",&m_MuonSeg_py);
  m_tree->Branch("MuonSeg_pz","vector<float>",&m_MuonSeg_pz); 
  m_tree->Branch("MuonSeg_t0","vector<float>",&m_MuonSeg_t0);
  m_tree->Branch("MuonSeg_t0error","vector<float>",&m_MuonSeg_t0error);
  m_tree->Branch("MuonSeg_chi2","vector<float>",&m_MuonSeg_chi2);
  m_tree->Branch("MuonSeg_ndof","vector<float>",&m_MuonSeg_ndof);
  m_tree->Branch("MuonSeg_sector","vector<int>",&m_MuonSeg_sector);
  m_tree->Branch("MuonSeg_chamberIndex","vector<int>",&m_MuonSeg_chamberIndex);
  m_tree->Branch("MuonSeg_nPrecisionHits","vector<int>",&m_MuonSeg_nPrecisionHits);
  m_tree->Branch("MuonSeg_theta_pos","vector<float>",&m_MuonSeg_theta_pos);
  m_tree->Branch("MuonSeg_theta_dir","vector<float>",&m_MuonSeg_theta_dir);


  m_tree->Branch(m_jetCollection+".n", &m_jets_n,"n/i");
  m_tree->Branch(m_jetCollection+".pt","vector<float>",&m_jets_pt);
  m_tree->Branch(m_jetCollection+".eta","vector<float>",&m_jets_eta);
  m_tree->Branch(m_jetCollection+".phi","vector<float>",&m_jets_phi);
  m_tree->Branch(m_jetCollection+".time","vector<float>",&m_jets_time);
  m_tree->Branch(m_jetCollection+".emf","vector<float>",&m_jets_emf);
  m_tree->Branch(m_jetCollection+".hecf","vector<float>",&m_jets_hecf);
  m_tree->Branch(m_jetCollection+".negE","vector<float>",&m_jets_negE);
  m_tree->Branch(m_jetCollection+".fracSamplingMax","vector<float>",&m_jets_fracSamplingMax);
  m_tree->Branch(m_jetCollection+".fracSamplingMaxIndex","vector<int>",&m_jets_fracSamplingMaxIndex);
  m_tree->Branch(m_jetCollection+".width","vector<float>",&m_jets_width);
  //m_tree->Branch(m_jetCollection+".n90","vetor<int>",&m_jets_n90);
  m_tree->Branch(m_jetCollection+".NumTrkPt1000","vector<int>",&m_jets_numTrkPt1000);
  
  if (m_trigTool.retrieve().isFailure()) {
    ATH_MSG_ERROR ("failed to retrieve the trigger tool");
    return StatusCode::FAILURE;
  }
  
 
  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ANA_MSG_INFO("in execute");

  // retrieve the eventInfo object from the event store
  // const xAOD::EventInfo *eventInfo = nullptr;
  //ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  //ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  const xAOD::EventInfo* eventInfo = 0;
  if (evtStore()->retrieve(eventInfo, "EventInfo").isFailure()) {
    ATH_MSG_INFO ("Failed to retrieve the EventInfo");
    return StatusCode::FAILURE;
  }

  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();

  const xAOD::MuonSegmentContainer* MuonSegments = 0;
  if (!evtStore()->retrieve(MuonSegments,"MuonSegments").isSuccess()) {
    ATH_MSG_INFO ("failed to retrieve the MuonSegments.");
    return StatusCode::FAILURE;
  }

  m_MuonSeg_x.clear();
  m_MuonSeg_y.clear();
  m_MuonSeg_z.clear(); 
  m_MuonSeg_px.clear();
  m_MuonSeg_py.clear();
  m_MuonSeg_pz.clear(); 
  m_MuonSeg_t0.clear();
  m_MuonSeg_t0error.clear();
  m_MuonSeg_chi2.clear();
  m_MuonSeg_ndof.clear();
  m_MuonSeg_sector.clear();
  m_MuonSeg_chamberIndex.clear();
  m_MuonSeg_nPrecisionHits.clear();
  m_MuonSeg_theta_pos.clear();
  m_MuonSeg_theta_dir.clear();
  
  for (const xAOD::MuonSegment* muonSeg : *MuonSegments) {
   
    m_MuonSeg_x.push_back(muonSeg->x());
    m_MuonSeg_y.push_back(muonSeg->y());
    m_MuonSeg_z.push_back(muonSeg->z());
    m_MuonSeg_px.push_back(muonSeg->px());
    m_MuonSeg_py.push_back(muonSeg->py());
    m_MuonSeg_pz.push_back(muonSeg->pz());
    m_MuonSeg_t0.push_back(muonSeg->t0());
    m_MuonSeg_t0error.push_back(muonSeg->t0error());
    m_MuonSeg_chi2.push_back(muonSeg->chiSquared());
    m_MuonSeg_ndof.push_back(muonSeg->numberDoF());
    m_MuonSeg_sector.push_back(muonSeg->sector());
    m_MuonSeg_chamberIndex.push_back(muonSeg->chamberIndex());
    m_MuonSeg_nPrecisionHits.push_back(muonSeg->nPrecisionHits());

    TVector3 pos(muonSeg->x(), muonSeg->y(), muonSeg->z());
    TVector3 dir(muonSeg->px(), muonSeg->py(), muonSeg->pz());

    m_MuonSeg_theta_pos.push_back(pos.Theta());
    m_MuonSeg_theta_dir.push_back(dir.Theta());
  }


  const xAOD::JetContainer* jets = 0;
  if (evtStore()->retrieve(jets, m_jetCollection.Data()).isFailure()) {
    ATH_MSG_ERROR ("failed to retrieve the jets");
    return StatusCode::FAILURE;
  }
  else {
      ATH_MSG_INFO ( "#jets: " << jets->size());

      m_jets_n = jets->size();
      m_jets_pt.clear();
      m_jets_eta.clear();
      m_jets_phi.clear();
      m_jets_time.clear();  
      m_jets_emf.clear();
      m_jets_hecf.clear();
      m_jets_negE.clear();
      m_jets_fracSamplingMax.clear();
      m_jets_fracSamplingMaxIndex.clear();
      m_jets_width.clear();
      //m_jets_n90.clear();
      m_jets_numTrkPt1000.clear();
   
      for (const xAOD::Jet* jet : * jets) {
	m_jets_pt.push_back(jet->pt());
	m_jets_eta.push_back(jet->eta());
	m_jets_phi.push_back(jet->phi());
	m_jets_time.push_back(jet->getAttribute<float>("Timing"));

	m_jets_emf.push_back(jet->getAttribute<float>("EMFrac"));
	m_jets_hecf.push_back(jet->getAttribute<float>("HECFrac"));
	m_jets_negE.push_back(jet->getAttribute<float>("NegativeE"));
	m_jets_fracSamplingMax.push_back(jet->getAttribute<float>("FracSamplingMax"));
	m_jets_fracSamplingMaxIndex.push_back(jet->getAttribute<int>("FracSamplingMaxIndex"));
	m_jets_width.push_back(jet->getAttribute<float>("Width"));
	//m_jets_n90.push_back(jet->getAttribute<int>("N90Cells")); ERROR SG::ExcBadAuxVar: Attempt to retrieve nonexistent aux data item `::N90Cells' 
	vector<int> numTrkPt1000;
	jet->getAttribute<vector<int>>("NumTrkPt1000", numTrkPt1000);
	if (numTrkPt1000.size())
	  m_jets_numTrkPt1000.push_back(numTrkPt1000.at(0));
      }
	  }
  
  /*   const xAOD::VertexContainer* primaryVertices = 0;
    if (!evtStore()->retrieve( primaryVertices, "PrimaryVertices").isSuccess()) {
    std::cout << "Error: failed to retrieve PrimaryVertices" << std::endl;
    return StatusCode::FAILURE;
  }
    m_NPV=0;
    for (size_t iVertex=0; iVertex< primaryVertices->size(); iVertex++)
    if ( primaryVertices->at(iVertex)->vertexType() == xAOD::VxType::VertexType::PriVtx || primaryVertices->at(iVertex)->vertexType() == xAOD::VxType::VertexType::PileUp)
      m_NPV++;   
  */
  std::vector<std::string> listOfTriggers = m_trigTool->getListOfTriggers();
  for (string item : listOfTriggers)
    if (m_trigTool->isPassed(item)) {
      // ATH_MSG_INFO ("passes " << item );
    
      bool new_item = true;
      unsigned int index = 0;
      for (; index< m_trigItems_names.size(); index++) 
        if (m_trigItems_names[index] == item) {
          new_item = false;
          break;
        }

      if (new_item) {
        m_trigItems_names.push_back(item);
        m_trigItems_counts.push_back(1);
      } else 
        m_trigItems_counts[index]++;
      
	}

  


  m_L1_J12 = m_trigTool->isPassed("L1_J12");
  m_tree->Fill();
  ATH_MSG_INFO("Tree Entries" << m_tree->GetEntries());  

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

    for (unsigned int i = 0; i < m_trigItems_names.size(); i++) 
  ATH_MSG_INFO (m_trigItems_names[i] << "   counts: " << m_trigItems_counts[i]);
  

  ATH_MSG_INFO("in finalize");
  m_tree->Write();  
  // gDirectory->cd(m_treeFolder);
  ATH_MSG_INFO("after write");
  m_file->Close();


  return StatusCode::SUCCESS;
}
