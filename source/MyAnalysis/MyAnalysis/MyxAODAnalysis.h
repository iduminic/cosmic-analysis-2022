#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>


//#include "GeneratorObjects/McEventCollection.h"
//#include "AthenaBaseComps/AthAlgorithm.h"
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TVector3.h>
#include <vector>
using namespace std;
#include <string>
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
//#include "GaudiKernel/ToolHandle.h"


class MyxAODAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
 
 private:
  // Configuration, and any other types of variables go here.

  ToolHandle<Trig::TrigDecisionTool> m_trigTool{"Trig::TrigDecisionTool"};
  vector<string> m_trigItems_names;
  vector<int> m_trigItems_counts;

  TFile* m_file;
  TTree* m_tree;
  TString m_treeFolder;

  unsigned int m_NPV;
  float m_actualInteractionsPerCrossing;
  float m_averageInteractionsPerCrossing;

  int m_L1_BCM_AC_CA_BGRP0; 
  int m_L1_BCM_Wide_BGRP0; 

  int m_L1_J12;

  unsigned int m_runNumber;
  unsigned int m_eventNumber;
  unsigned int m_backgroundFlags;
  unsigned int m_bcid;

  unsigned int m_cl_n;
  vector<float> m_cl_pt;
  vector<float> m_cl_e;
  vector<float> m_cl_eta;
  vector<float> m_cl_phi;
  vector<float> m_cl_rPerp;
  vector<float> m_cl_z;
  vector<float> m_cl_time;
  vector<float> m_cl_FracSamplingMax;
  vector<int>   m_cl_FracSamplingMaxIndex;

  unsigned int m_jets_n;
  vector<float> m_jets_pt;
  vector<float> m_jets_eta;
  vector<float> m_jets_phi;
  vector<float> m_jets_time;

  vector<float> m_jets_emf;
  vector<float> m_jets_hecf;
  vector<float> m_jets_negE;
  //vector<float> m_jets_chf;
  vector<float> m_jets_fracSamplingMax;
  vector<int> m_jets_fracSamplingMaxIndex;
  vector<float> m_jets_width;
  //vector<int> m_jets_n90;
  vector<int> m_jets_numTrkPt1000;

  vector<double> m_truthEvent_weights;

  vector<float> m_NCB_MuonSeg_x;
  vector<float> m_NCB_MuonSeg_y;
  vector<float> m_NCB_MuonSeg_z;
  vector<float> m_NCB_MuonSeg_px;
  vector<float> m_NCB_MuonSeg_py;
  vector<float> m_NCB_MuonSeg_pz;
  vector<float> m_NCB_MuonSeg_t0;
  vector<float> m_NCB_MuonSeg_t0error;
  vector<float> m_NCB_MuonSeg_chi2;
  vector<float> m_NCB_MuonSeg_ndof;
  vector<int> m_NCB_MuonSeg_sector;
  vector<int> m_NCB_MuonSeg_chamberIndex;
  vector<int> m_NCB_MuonSeg_nPrecisionHits;
  vector<float> m_NCB_MuonSeg_theta_pos;
  vector<float> m_NCB_MuonSeg_theta_dir;

  vector<float> m_MuonSeg_x;
  vector<float> m_MuonSeg_y;
  vector<float> m_MuonSeg_z;
  vector<float> m_MuonSeg_px;
  vector<float> m_MuonSeg_py;
  vector<float> m_MuonSeg_pz;
  vector<float> m_MuonSeg_t0;
  vector<float> m_MuonSeg_t0error;
  vector<float> m_MuonSeg_chi2;
  vector<float> m_MuonSeg_ndof;
  vector<int> m_MuonSeg_sector;
  vector<int> m_MuonSeg_chamberIndex;
  vector<int> m_MuonSeg_nPrecisionHits;
  vector<float> m_MuonSeg_theta_pos;
  vector<float> m_MuonSeg_theta_dir;

  TString m_jetCollection;


};

#endif
