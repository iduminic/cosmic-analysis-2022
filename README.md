//First, you need to transform the xAOD files into n-tuples.
//Establish the ATLAS environment in which you are working. 

setupATLAS

//We create a new directory in which we will work.

mkdir ROOTAnalysisTutorial
cd ROOTAnalysisTutorial
mkdir source
mkdir build
mkdir run

//Inside the source directory we need a file CMakeLists.txt which will essentially tell cmake that this is a “normal” ATLAS work area. Let’s create this file as source/CMakeLists.txt.

cd build/
asetup AnalysisBase 22.2.76
cmake ../source/
make

//Recompiling, every time you make a modification:

cd ../build/
make

//Let's organize the working/analysis directory:

cd ../source/
mkdir MyAnalysis
mkdir MyAnalysis/MyAnalysis
mkdir MyAnalysis/Root
mkdir MyAnalysis/src
mkdir MyAnalysis/src/components
mkdir MyAnalysis/share

//We create a new CMakeLists.txt in the MyAnalysis directory.
//Recompile and source: 

cd ../build/
cmake ../source/
source x86_64-*/setup.sh
make

//Very Important: Since you created a new package you also must call in the build directory:

source x86_64-*/setup.sh //if you put star*, it gets the entire name of the directory

//Going into your source/ directory, create the file MyAnalysis/MyAnalysis/MyxAODAnalysis.h with the appropriate content.

//Create the file MyAnalysis/Root/MyxAODAnalysis.cxx

//The last step is to run the following commands:

cd ../run
athena MyAnalysis/ATestRun_jobOptions.py

//If it does not work, you can try and erase all cache (CMakeCache.txt) and build the configuration again. Another way of solving this issue is to delete with

rm -rf build

//the whole build directory and construct it from scratch. If you implement one of this 2 solutions, it should work fine. Then, I hope you can see some plots.

mkdir build
cd build
asetup AnalysisBase 22.2.76 //10 months update - try to use AthAnalysis 22.2.76 instead
cmake ../source/
source x86_64-centos7-gcc11-opt/setup.sh
make
cd ../run
athena MyAnalysis/ATestRun_jobOptions.py

//If it does not work from the first try, you should consider removing and recreating the build directory again. Also, try logging out of the server and connecting again. I don't know why this happens, but if you follow the same steps, it should eventually work fine. So, try the same thing a few times, because you don't really know when it will work.
